#!/usr/bin/env guile
!#
(use-modules (mkdir-p)
             (srfi srfi-19))

(define dotfiles
  '(.emacs
    .exwm
    .config/dunst/dunstrc))

(define dotfiles-directory (string-append (getenv "HOME") "/" "dotfiles"))
(define backup-suffix (date->string (current-date) ".~Y~m~d~H~M~S"))

(define (symlink-dotfile dotfile)
  (let ((oldpath (string-append dotfiles-directory "/" (symbol->string dotfile)))
        (newpath (string-append (getenv "HOME") "/" (symbol->string dotfile))))
    (cond
     ((string-index-right newpath #\/)
      => (lambda (i) (mkdir-p (substring newpath 0 i)))))
    (call/cc (lambda (skip)
               (when (file-exists? newpath)
                 (when (string=? oldpath (readlink newpath))
                   (skip))
                 (rename-file newpath (string-append newpath backup-suffix)))
               (symlink oldpath newpath)))))

(for-each symlink-dotfile dotfiles)

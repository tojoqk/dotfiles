;;; Indent
(setq-default indent-tabs-mode nil)

;;; GUI
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)

;;; ido
(ido-mode 1)
(ido-everywhere 1)

;;; ido-completing-read+
(ido-ubiquitous-mode 1)

;;; smex
(global-set-key (kbd "M-x") 'amx)
(global-set-key (kbd "M-X") 'amx-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;;; ido-at-point-mode
(ido-at-point-mode 1)

;;; icomplete
(require 'icomplete)
(icomplete-mode 1)

;;; Projectile
(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;;; Magit
(setq magit-completing-read-function 'magit-ido-completing-read)

;;; Org
(setq org-completion-use-ido t)
(setq org-directory "~/org")
(setq org-agenda-files (list org-directory))
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c c") 'org-capture)
(setq org-default-notes-file (concat org-directory "/notes.org"))
(add-hook 'org-mode-hook
          (lambda ()
            (set-face-attribute 'org-document-title nil :height 1.4)
            (set-face-attribute 'org-level-1 nil :height 1.3)
            (set-face-attribute 'org-level-2 nil :height 1.2)
            (set-face-attribute 'org-level-3 nil :height 1.1)))
(setq org-capture-templates '(("c" "Note" entry (file+headline "" "Notes")
                               "* %?\n  :PROPERTIES:\n  :CREATED: %U\n  :END:\n  %u\n  %a"
                               :empty-lines 1)
                              ("l" "Log" entry (file "~/org/log.org")
                               "* %?\n  :PROPERTIES:\n  :CREATED: %U\n  :END:\n"
                               :empty-lines 1)
                              ("t" "Tasks" entry (file+headline "" "Tasks")
                               "* TODO %?\n  :PROPERTIES:\n  :CREATED: %U\n  :END:\n  %a"
                               :empty-lines 1)))
(setq org-todo-keywords
      '((sequence "TODO(t)" "WAIT(w)" "|" "DONE(d)" "SOMEDAY(s)" "CANCELED(c)")))
(org-babel-do-load-languages
 'org-babel-load-languages
 '((shell . t)
   (emacs-lisp . t)
   (scheme . t)
   (eshell . t)))
(setq org-confirm-babel-evaluate nil)

;;; org-bullets
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;;; SKK
(setq default-input-method "japanese-skk")
(setq skk-large-jisyo "~/.emacs.d/skk-get-jisyo/SKK-JISYO.L")

;;; Theme
(load-theme 'doom-dracula t)

;;; modeline
(doom-modeline-init)
(setq doom-modeline-project-detection 'projectile)
(setq doom-modeline-buffer-file-name-style 'auto)
(setq doom-modeline-gnus t)
(setq doom-modeline-gnus-timer 2)
(display-battery-mode 1)
(display-time-mode 1)
(setq display-time-default-load-average t)

;;; undo-tree-mode
(global-undo-tree-mode t)

;;; which-key
(which-key-mode)

;;; Font
(set-face-attribute 'default nil :family "Hermit"  :height 100)
(set-fontset-font (frame-parameter nil 'font)
                  'japanese-jisx0208
                  (font-spec :family "Noto Sans Mono CJK JP"
                             :size 16))

;;; Paredit
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)

;;; Lisp
(add-hook 'lisp-mode-hook
	  (lambda ()
	    (set (make-local-variable 'lisp-indent-function)
		 'common-lisp-indent-function)))

;;; Guix
(when (file-directory-p "~/src/guix")
  (with-eval-after-load 'geiser-guile
    (add-to-list 'geiser-guile-load-path "~/src/guix"))
  (with-eval-after-load 'yasnippet
    (add-to-list 'yas-snippet-dirs "~/src/guix/etc/snippets"))
  (setq user-full-name "Masaya Tojo")
  (setq user-mail-address "masaya@tojo.tokyo")
  (load-file "~/src/guix/etc/copyright.el")
  (setq copyright-names-regexp
        (format "%s <%s>" user-full-name user-mail-address))
  (add-hook 'after-save-hook 'copyright-update))

;;; ACL2
(when (file-directory-p "~/acl2-8.3")
  (load "~/acl2-8.3/emacs/emacs-acl2.el")
  (put 'defthm 'lisp-indent-function 1))

;;; Geiser
(setq geiser-active-implementations '(guile))

;;; Gnus
(setq gnus-completing-read-function 'gnus-ido-completing-read)
